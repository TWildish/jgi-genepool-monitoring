#
# Build/push the shiny container:
docker build -t tonywildish/gpmon-shiny -f Dockerfile.gpmon-shiny .
docker push tonywildish/gpmon-shiny

#
# Run the shiny app!
docker run --rm \
  --name gpmon-shiny \
  --link gpmon-mysqld:gpmon-shiny \
  --detach \
  tonywildish/gpmon-shiny
